package io.alexrintt.howmanylines.utils

import io.alexrintt.howmanylines.services.Github
import io.alexrintt.howmanylines.services.OkHttpClient

/// Http and GitHub services
val http = OkHttpClient()
val github = Github(http)
