package io.alexrintt.howmanylines.interfaces

abstract class MarkdownBuilder {
  abstract fun build(): String
}